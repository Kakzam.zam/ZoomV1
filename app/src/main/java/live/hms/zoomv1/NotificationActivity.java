package live.hms.zoomv1;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import live.hms.zoomv1.databinding.ActivityMainBinding;
import live.hms.zoomv1.network.RetrofitServerCallback;
import live.hms.zoomv1.presenter.RestPresenter;

public class NotificationActivity extends AppCompatActivity {

    /* getting layout ActivityMain, so in one Activity with MainActivity */
    ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        if (getIntent() != null) {
            /* getting getIntent from FirebaseServiceMessage which has get in FCM
            *  and display to Toast */
            Toast.makeText(this, "Title : " + getIntent().getStringExtra("title") + "\n Message : " + getIntent().getStringExtra("message"), Toast.LENGTH_LONG).show();
        }

        binding.nama.setHint("Title");
        binding.link.setHint("Message");
        binding.token.setText("PUSH");
        binding.join.setVisibility(View.GONE);
        binding.mute.setVisibility(View.GONE);
        binding.mic.setVisibility(View.GONE);
        binding.leave.setVisibility(View.GONE);
        binding.updatePeserta.setVisibility(View.GONE);

        binding.token.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /* send Message to FCM by token */
                new RestPresenter().sendMessage(
                        /* get Title Message FCM */
                        binding.nama.getText().toString(),
                        /* get Body Message FCM */
                        binding.link.getText().toString(),
                        new RetrofitServerCallback() {
                    @Override
                    public void onSuccess(String response) {
                        Log.v("ZAM", "sendMessage onSuccess: " + response);
                    }

                    @Override
                    public void onFailed(String response) {
                        Log.v("ZAM", "sendMessage onFailed: " + response);
                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                        Log.v("ZAM", "sendMessage onFailure: " + throwable.getMessage());
                    }
                });
            }
        });
    }
}